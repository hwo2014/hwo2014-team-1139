StraightPiece = Struct.new(:length, :switch?)
class StraightPiece
  alias_method :orig_length, :length
  def length(start_lane, end_lane)
    if switch?
      # approximation, really some kind of spline (at least on map)
      Math.sqrt(orig_length**2 + (start_lane.distance_from_center - end_lane.distance_from_center)**2)
    else
      orig_length
    end
  end

  def inspect
    "Straight#{switch? ? ' switch ' : ''}, length #{'%.2f' % orig_length}"
  end
end

CurvePiece = Struct.new(:radius, :angle, :switch?)
class CurvePiece
  def length(start_lane, end_lane)
    # lol
    adj_radius = 0.5 * adjusted_radius(start_lane) + 0.5 * adjusted_radius(end_lane)
    (angle.abs / 360.0) * 2 * Math::PI * adj_radius
  end

  def adjusted_radius(lane)
    radius + (angle > 0 ? -lane.distance_from_center : lane.distance_from_center)
  end

  def inspect
    "Curve#{switch? ? ' switch ' : ''}, angle #{'%.2f' % angle}, radius #{'%.2f' % radius}"
  end
end
Lane = Struct.new(:index, :distance_from_center)
class Lane
  def inspect
    "Lane #{index}, distance #{distance_from_center}"
  end
end
CarPosition = Struct.new(:piece_index, :piece_distance, :start_lane, :end_lane, :lap, :travelled_pieces_distance)
class CarPosition
  def track_distance
    travelled_pieces_distance + piece_distance
  end
end
CarDimensions = Struct.new(:length, :width, :guide_flag_position)
Car = Struct.new(:name, :color, :dimensions, :angle, :position, :pos_history)
class Car

  def speed
    # Arvio keskimääräisestä nopeudesta viimeisen POSITION_HISTORY_MAX_LENGTH perusteella
    # Varmaan voisi painottaa uusimpia tietoja enemmän kuin vanhoja..

    # Jontte!
    if pos_history.length < 2
      return 0
    end
    accum = 0

    pos_history.each_index do |i|

      if i+1 >= pos_history.length
        break
      end

      piece_cur = pos_history[i].track_distance
      piece_next = pos_history[i+1].track_distance

      accum += (piece_next - piece_cur).abs
    end
    accum / (pos_history.length - 1)

  end
  def speed_delta
    # Ruma arvio nopeuden muutoksesta viimeisimmän askeleen perusteella vrt. keskimääräinen nopeus
    if pos_history.length < 2
      return 0
    end

    l = pos_history.length
    latest_speed = (pos_history[l-2].track_distance - pos_history[l-1].track_distance).abs

    latest_speed - speed
  end
end
