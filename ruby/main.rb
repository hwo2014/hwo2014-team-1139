# kate: space-indent on; indent-width 2; mixedindent off;

require 'json'
require 'socket'
require_relative 'types'

class Integer
  def fmt2
    to_s.rjust(2, '0')
  end
end

server_host = ARGV[0]
server_port = ARGV[1]
username = ENV['USER'] || 'ci'
bot_name = "#{username[0,6]}-#{Time.now.month.fmt2}#{Time.now.day.fmt2}#{Time.now.hour.fmt2}#{Time.now.min.fmt2}"
bot_key = ARGV[3]

POSITION_HISTORY_MAX_LENGTH = 4
PARAM_PERSISTENCE = 0.9
# If car is closer than this on our current lane, try to change
LANE_LOOKAHEAD_DISTANCE = 150

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

def clamp (x, min, max)
  if x < min
    return min
  end
  if x > max
    return max
  end
  return x
end

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    @ticks = 0
    @cars = {}
    @bot_name = bot_name

    filename = "jslog/log.txt"
    @fp = File.open(filename, "w")
    @annofp = File.open("jslog/annotation.txt", "w")
    @fp << "tick angle lap pieceIndex pieceLength pieceRadius inPieceDistance position startLaneIndex endLaneIndex speed target throttle\n"
    @annofp << "tick\tmessage\n"

    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end
  attr_reader :pieces, :lanes

  def me
    @cars[@bot_name]
  end

  
  def effective_turbo
    if @turboActive then 
      return @turboFactor
    end
    1
  end
  def ai_start
    @accel = true
    @turboAvailable = false
    @turboFactor = 1
    @turboDuration = 0
    @turboActive = false
    @turboTick = -1 # When to deploy
    @turboEffectivenessHeuristic = 1.0 # the sooner we use it the better. 
    
    @lane_switch_state = :none
    
    @k1 = 0.2
    @k2 = 0.02

    @previous_throttle = 0
    @speeds = []
    @throttle_delay = 2

    @throttles = [0,0]
    chart = ARGV[4]

    @multiplier = 6.9
    if chart == 'france'
        @multiplier = 7.1
    elsif chart == 'germany'
        @multiplier = 7.0
    end
  end

  def time_to_new_speed(old_speed, new_speed, turbo_multiplier)
    
    throttle = 0
    if new_speed > old_speed then
      throttle = 1
    end
    
    Math.log((@k1*throttle*turbo_multiplier-@k2*new_speed)/(@k1*throttle*turbo_multiplier-@k2*old_speed))/Math.log(1-@k2)
  end
  
  def distance_covered(start_speed, throttle, ticks)
    # Return distance traveled during constant throttle given starting speed and number of ticks
    (@k2 * start_speed * ((1-@k2)**ticks-1)+@k1*(-throttle * (1-@k2)**ticks + throttle * ticks * Math.log(1-@k2) + throttle))/(@k2 * Math.log(1-@k2))
  end
  
  def speed_after_acceleration(start_speed, throttle, ticks)
    ((1-@k2)**ticks*(@k2 * start_speed - @k1 * throttle)+@k1*throttle)/@k2
  end

  def throttle_ai
    cur_piece = pieces[me.position.piece_index]
    next_piece = pieces[(me.position.piece_index + 1) % pieces.length]
    lane0 = lanes[me.position.start_lane]
    lane1 = lanes[me.position.end_lane]

    target_speeds = [{:speed => 200, :distance => 0}]

    dist_accum = 0.0
    (me.position.piece_index..me.position.piece_index+10).each do |piece_idx|
      #if dist_accum > 150
      #  break
      #end

      piece = pieces[piece_idx % pieces.length]

      case piece
      when CurvePiece
        curve_radius = piece.adjusted_radius(lane1)
        ts = @multiplier * (curve_radius / 110.0)**0.8
        #if dist_accum > 80
        #  ts *= 1.0 + (dist_accum - 80) / 100.0
        #end
        #target_speeds << ts
      when StraightPiece
        #target_speeds << 200
        ts = 200
      end

      target_speeds << {:speed => ts, :distance => dist_accum}
      
      if piece_idx == me.position.piece_index
        dist_accum += piece.length(lane0, lane1) - me.position.piece_distance
      else
        dist_accum += piece.length(lane1, lane1)
      end

    end

    @target_speed = 200
    # Pick target speed based on next limiting track piece speed
    
    turbo_useful = false
    limiting_piece = nil
    
    target_speeds.each do |entry|
      
      entry_speedlimit = entry[:speed]
      entry_distance = entry[:distance]
      
      if entry_distance < 0.1 then
        if entry_speedlimit < @target_speed then
          @target_speed = entry_speedlimit
        end
      elsif entry_speedlimit < me.speed then
        
        # Calculate braking time
        t = time_to_new_speed me.speed, entry_speedlimit, 0
        
        # Calculate braking distance
        dist = distance_covered me.speed, 0, t
        
        if dist+1 > entry_distance and entry_speedlimit < @target_speed then
          @target_speed = entry_speedlimit
          limiting_piece = entry
#           puts "dist to target: #{entry_distance}, limit: #{entry_speedlimit}"
        end
      end
      
      if @turboAvailable then
        # Cool, what if we used turbo?
        
        # Velocity after acc
        speed_after = speed_after_acceleration me.speed, @turboFactor, @turboDuration*@turboEffectivenessHeuristic
        dist = distance_covered me.speed, @turboFactor, @turboDuration*@turboEffectivenessHeuristic

#         puts "speed after turbo #{speed_after}, acc dist: #{dist}"
        
        # Time to brake
        if entry_speedlimit < speed_after then
          t = time_to_new_speed speed_after, entry_speedlimit, 0
          if t > 0 then
            dist += distance_covered speed_after, 0, t
#             puts "turbo acc+decc dist #{dist}, time: #{t}"
            
            if dist+1 < entry_distance then
              turbo_useful = true
            end
          end
        end
       end
    end
    
    if @turboAvailable and limiting_piece == nil and turbo_useful then
      @turboTick = @ticks # engage turbo
    end

    current_speed = me.speed || 0
    current_speed_delta = me.speed_delta || 0
    estimated_speed = speed_after_acceleration (speed_after_acceleration current_speed, @throttles[-2]*effective_turbo, 1), @throttles[-1]*effective_turbo, 1

    speed_error = @target_speed - current_speed

    if (@target_speed-estimated_speed).abs < 0.1 
        throttle = @target_speed * @k2 / (@k1 * effective_turbo)
        puts "using estimated speed!"
    elsif speed_error > 0.1
       throttle = 1
    elsif speed_error < -0.1
       throttle = 0
    else
       throttle = @target_speed * @k2 / (@k1 * effective_turbo)
    end
#
#    if me.speed > 4.9
#       throttle = 0
#       @accel = false
#    elsif me.speed < 0.1
#       throttle = 0.5
#       @accel = true
#    elsif @accel 
#       throttle = 0.5
#    else
#       throttle = 0
#    end 
#     $stderr.puts "#{@k1} #{@k2}"
    
    if throttle == @previous_throttle and not @turboActive then
      @speeds << current_speed
      if @speeds.length > 3 then
        @speeds = @speeds[1,4]
        
        x0 = @speeds[0]
        x1 = @speeds[1]
        x2 = @speeds[2]
  
#         uusi_nopeus = vanha_nopeus + k1*throttle - k2*vanha_nopeus
        if throttle * (x1-x0) != 0 then
          k1 = (x1**2 - x0*x2)/(throttle * (x1-x0))
          k2 = (-x0+2*x1-x2) / (x1-x0)
          
          if k1 > 0 and k2 > 0 and k1 < 1 and k2 < 1 then
            @k1 = PARAM_PERSISTENCE * @k1 + (1.0 - PARAM_PERSISTENCE) * k1
            @k2 = PARAM_PERSISTENCE * @k2 + (1.0 - PARAM_PERSISTENCE) * k2
          end
        end
      end
    else
      @speeds = []
    end
    
    throttle = clamp throttle, 0, 1
    @previous_throttle = throttle
    puts "speed: #{current_speed}, target: #{@target_speed}, estimated: #{estimated_speed} E: #{speed_error}, throttle: #{throttle}"
    puts ""
    throttle
  end

  def lane_availability
    cur = me.position.end_lane

    blocking_l = []
    blocking_c = []
    blocking_r = []

    @cars.each_pair do |name, car|
      next if name == @bot_name

      if car.position.end_lane == cur
        blocking_c << car
      elsif car.position.end_lane == cur-1
        blocking_l << car
      elsif car.position.end_lane == cur+1
        blocking_r << car
      end
    end

    closest_c = (blocking_c.map { |car| car.position.track_distance }.select { |d| d > me.position.track_distance }.min || 1000000) - me.position.track_distance
    if closest_c > LANE_LOOKAHEAD_DISTANCE
      return :no_change
    end

    closest_l = (blocking_l.map { |car| car.position.track_distance }.select { |d| d > me.position.track_distance }.min || 1000000) - me.position.track_distance
    closest_r = (blocking_r.map { |car| car.position.track_distance }.select { |d| d > me.position.track_distance }.min || 1000000) - me.position.track_distance

    closest_l = -1 if cur == 0
    closest_r = -1 if cur == @lanes.length-1

    if closest_r > closest_l and closest_r > closest_c
      return :change_right
    elsif closest_l > closest_r and closest_l > closest_c
      return :change_left
    else
      return :no_change
    end
  end

  def react_to_messages_from_server(tcp)
    ai_start
    while json = tcp.gets
      message = JSON.parse(json)
      msg_type = message['msgType']
      msg_data = message['data']
      case msg_type
      when 'carPositions'
        @ticks = message['gameTick'] || 0
        msg_data.each do |car|
          name = car['id']['name']
          car_s = @cars[name]
          car_s.angle = car['angle']
          car_s.pos_history << car_s.position if car_s.position
          car_s.pos_history.shift if car_s.pos_history.length > POSITION_HISTORY_MAX_LENGTH

          new_pos = CarPosition.new(
            car['piecePosition']['pieceIndex'],
            car['piecePosition']['inPieceDistance'],
            car['piecePosition']['lane']['startLaneIndex'],
            car['piecePosition']['lane']['endLaneIndex'],
            car['piecePosition']['lap'],
            car_s.position.travelled_pieces_distance
          )
          if new_pos.piece_index != car_s.position.piece_index
            prev_piece_dist = pieces[car_s.position.piece_index].length(
                lanes[car_s.position.start_lane], lanes[car_s.position.end_lane])
            new_pos.travelled_pieces_distance += prev_piece_dist
          end

          car_s.position = new_pos
        end

        if @lane_switch_state == :none
          @lane_choice = lane_availability
        end

        throttle = throttle_ai

        @throttles << throttle
        current_piece = pieces[me.position.piece_index]
        log_vars = [
          @ticks,
          me.angle,
          me.position.lap,
          me.position.piece_index,
          current_piece.length(@lanes[me.position.start_lane], @lanes[me.position.end_lane]),
          current_piece.is_a?(CurvePiece) ? current_piece.adjusted_radius(@lanes[me.position.start_lane]) : 0.0, # doesn't take into account curve switches
          me.position.piece_distance,
          me.position.track_distance,
          me.position.start_lane,
          me.position.end_lane,
          me.speed,
          @target_speed,
          throttle
          ]
        if @ticks > 0
           @fp << log_vars.join(' ') << "\n"
           @fp.flush
        end
        @turboEffectivenessHeuristic *= 0.997692 # effectiveness halves every 5*60 ticks
        
        if @lane_switch_state == :waiting
          if me.position.start_lane != me.position.end_lane 
            @lane_switch_state == :switching
          end
        elsif @lane_switch_state == :switching
          if me.position.start_lane == me.position.end_lane
            @lane_switch_state = :none
          end
        end
        
        if @lane_choice != :no_change then
          tcp.puts switch_lange_message(@lane_choice)
          @lane_switch_state == :waiting
          @lane_choice = :no_change
          
        elsif @turboTick >= 0 and @turboTick == @ticks then
          tcp.puts turbo_message
        else
          tcp.puts throttle_message(throttle)
        end
      else
        case msg_type
        when 'join'
          puts 'Joined'
          @annofp << "#{@ticks}\tJoined\n"
        when 'gameInit'
          track = msg_data['race']['track']
          @pieces = track['pieces'].map do |piece|
            if piece['length']
              StraightPiece.new(piece['length'], !!piece['switch'])
            elsif piece['radius']
              CurvePiece.new(piece['radius'], piece['angle'], !!piece['switch'])
            else
              raise 'Unknown piece type'
            end
          end
          @lanes = track['lanes'].map do |lane|
            Lane.new(lane['index'], lane['distanceFromCenter'])
          end
          msg_data['race']['cars'].each do |car|
            dimensions = CarDimensions.new(car['dimensions']['length'],car['dimensions']['width'],car['dimensions']['guideFlagPosition'])
            name =car['id']['name']
            @cars[name] = Car.new(name, car['id']['color'], dimensions, 0, CarPosition.new(0,0,0,0,0,0), [CarPosition.new(0,0,0,0,0,0)])
          end

          File.open("track-#{track['id']}.mrh", "wb") do |fp|
            Marshal.dump({pieces: @pieces, lanes: @lanes}, fp)
          end

        when 'gameStart'
          puts 'Race started'
        when 'crash'
          @annofp << "#{@ticks}\t#{msg_data['name']} crashed\n"
          puts "#{msg_data} crashed"
          #speed_per_radius = me.speed / current_piece.adjusted_radius(@lanes[me.position.start_lane])
          #speed_squared_per_radius = speed_per_radius*me.speed
          #puts "Angle: #{me.angle} Speed / radius: #{speed_per_radius} Speed^2 / radius: #{speed_squared_per_radius}"
        when 'gameEnd'
          puts 'Race ended'
        when 'error'
          puts "ERROR: #{msg_data}"
        when 'turboAvailable'
          @turboDuration = msg_data['turboDurationTicks']
          @turboFactor = msg_data['turboFactor']
          @turboAvailable = true
          @turboEffectivenessHeuristic = 1
          puts "turboAvailable, factor: #{@turboFactor}, ticks: #{@turboDuration}"
        when 'turboStart'
          plr = msg_data['name']
          p msg_data
          if plr == @bot_name then
            @turboActive = true
            @turboAvailable = false
          end
        when 'turboEnd'
          plr = msg_data['name']
          p msg_data
          if plr == @bot_name then
            @turboActive = false
          end
        when 'dnf'
          @annofp << "#{@ticks}\t#{msg_data['car']['name']} DNF: #{msg_data['reason']}\n"
          puts "DNF: #{msg_data}"
        else
          @annofp << "#{@ticks}\tUnknown msg #{msg_type}\n"
        end
        @annofp.flush
        puts "Got #{msg_type}"
        tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
    #make_msg("createRace", botId: {name: bot_name, key: bot_key}, trackName: ARGV[4], password: 'asd', carCount: 1)
    #make_msg("joinRace", botId: {name: bot_name, key: bot_key}, trackName: ARGV[4], password: 'futurace', carCount: 1)
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end
  
  def turbo_message
    make_msg("turbo", "tankissa es")
  end
  
  def switch_lange_message(dir)
    if dir == :change_right then
      return make_msg("switchLane", "Right")
    else
      return make_msg("switchLane", "Left")
    end
  end
  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

if __FILE__ == $0
  NoobBot.new(server_host, server_port, bot_name, bot_key)
end

