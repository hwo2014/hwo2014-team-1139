require_relative 'types'

# Thoughts about lane selection
# - shortest path might not be better, since it contains tighter curves and therefore
#   the speed must be slower?

class LaneGraph
  def initialize(data)
    @data = data
    generate_graph data
  end
  attr_reader :graph, :data

  Pos = Struct.new(:piece, :lane)
  def generate_graph(data)
    @cost = {}

    @data[:pieces][0..-2].each_with_index do |piece, piece_idx|
      if piece.switch?
        @data[:lanes].each_with_index do |lane, lane_idx|
          if lane_idx > 0
            @cost[[Pos.new(piece_idx, lane_idx), Pos.new(piece_idx+1, lane_idx-1)]] =
                piece.length(lane, @data[:lanes][lane_idx-1])
          end
          @cost[[Pos.new(piece_idx, lane_idx), Pos.new(piece_idx+1, lane_idx)]] =
                piece.length(lane, lane)
          if lane_idx < @data[:lanes].length-1
            @cost[[Pos.new(piece_idx, lane_idx), Pos.new(piece_idx+1, lane_idx+1)]] =
                piece.length(lane, @data[:lanes][lane_idx+1])
          end
        end
      else
        @data[:lanes].each_with_index do |lane, lane_idx|
          @cost[[Pos.new(piece_idx, lane_idx), Pos.new(piece_idx+1, lane_idx)]] =
              piece.length(lane, lane)
        end
      end
    end
  end

  def dump_dot
    puts "digraph G {"
    @data[:pieces].each_with_index do |piece, piece_idx|
      @data[:lanes].each_with_index do |lane, lane_idx|
        desc = case piece
        when StraightPiece
          "S #{piece_idx}/#{lane_idx}"
        when CurvePiece
          "C #{piece_idx}/#{lane_idx}"
        end
        puts "p#{piece_idx}l#{lane_idx} [label=\"#{desc}\"];"
      end
    end
    @cost.each_pair do |(from, to), length|
      puts "p#{from.piece}l#{from.lane} -> p#{to.piece}l#{to.lane} [label=\"#{'%.2f' % length}\"];"
    end
    puts "}"
  end
end

if __FILE__ == $0
  lanegraph = LaneGraph.new(Marshal.load(File.read('track-keimola.mrh')))
  lanegraph.dump_dot
end
